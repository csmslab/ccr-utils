import datetime
from fabric import Connection
import os
import pickle
from pathlib import Path
from shlex import quote
import time
import uuid


class CCRShell:
    """
    Whitelisted commands that can be performed on a remote server running
    SLURM.

    Args:
        username (str): The username used to log into the server

        host (str): The address of the server

        transfer_host (str): The address of the server used to transfer large
            files.  If set to None, then use `host` above to transfer files.
            Optional, default = None.

        keyfile (str): The local path to the SSH key file used to log into the
            server. Optional, default = "~/.ssh/id_rsa".
    """

    def __init__(self, username, host, transfer_host = None, 
            keyfile = "~/.ssh/id_rsa"): 

        self.username = username
        self.host = host

        if transfer_host is None:
            self.transfer_host = self.host
        else:
            self.transfer_host = transfer_host

        # establish connection to host and trasfer host
        connect_kwargs = { "key_filename": keyfile }
        self.__con = Connection(self.host, user=self.username,
                connect_kwargs=connect_kwargs)
        self.__transfer_con = Connnection(self.transfer_host,
                user=self.username, connect_kwargs=connect_kwargs)


    # SLURM commands
    def squeue(self, job):
        cmd = "squeue -j {}".format(quote(job.job_id))
        return _exec(cmd)

    def squeue_user(self, user):
        cmd = "squeue -u {}".format(quote(user))
        return _exec(cmd)

    def sbatch(self, submit_file_name):
        cmd = "sbatch {}".format(quote(submit_file_name))
        return _exec(cmd)

    # File system commands

    def mkdir(self, dir_name, local = False):
        cmd = "mkdir {}".format(quote(dir_name))
        return self._exec(cmd, local)

    def rmdir(self, dir_name, local = False):
        cmd = "rm -rf {}".format(quote(dir_name))
        return self._exec(cmd, local)

    def tgz(self, tar_file_name, dir_name, local = False):
        cmd = "tar -czf {} -C {} .".format( 
                quote(tar_file_name), quote(dir_name))
        return self._exec(cmd, local)

    def txf(self, tgz_file_name, dir_name, local = False):
        cmd = "tar -xf {} -C {}".format( quote(tgz_file_name), quote(dir_name))
        return self._exec(cmd, local)

    def git_clone(self, branch, url, src_dir, local = False):
        cmd = "git clone -b {} {} {}".format(
                quote(branch), quote(url), quote(src_dir))
        return self._exec(cmd, local)

    def cat(self, txt, file_name, local = False):
        cmd = "cat \"{}\" > {}".format(
                quote(txt), quote(filename))
        return self._exec(cmd, local)


    # File transfer functions between local and remote server
    
    def put(self, src, trgt):
        self.__transfer_con.put(src, trgt)

    def get(self, src, trgt):
        self.__transfer_con.get(src, trgt)

    # Misc. functions
        
    def close():
        self.con.close()
        self.__transfer_con.close()

    def _exec(cmd, local = False):
        if local:
            res = self.__con.local(cmd, hide='stdout')
        else:
            res = self.__con.run(cmd, hide='stdout')
        return res


class CCRSubmit:
    """ 
    Python interface to CCR login node to submit and retrieve massively
    parallel jobs.


    Args:
        sh (CCRShell): A ccr shell object representing the connection to ccr.

        local_temp_dir (str): The location on local computer to place
            various files temporarily. Optional, default = "/tmp"

        verbose (bool): A flag to indicate whether to print status messages
    """

    def __init__(self, sh, local_temp_dir = "/tmp", verbose = False):
        self.verbose = verbose
        self.sh = sh 

        # create temporary local folder to create files 
        self.local_temp_dir = Path(local_temp_dir \
                + "/ccrsub." + str(uuid.uuid1()))
        self.sh.mkdir(self.local_temp_dir, local = True)


    def cleanup(self):
        """ Performs cleanup of directories """

        # remove local dir
        self.sh.rmdir(self.temp_dir, local = True)


    def setup(self, job):
        """ 
        Sets up working directory and required scripts 

        Args:
            job (CCRJob): The job to submit
        """

        self._setup_working_dir(job)
        self._write_run_script(job)
        self._write_submit_script(job)


    def submit(self, job, wait=False):
        """ Submits a job to the CCR scheduler.

        A wrapper to the `sbatch` command using the submit script built during
        setup. This sets the `job_id` field of the job, so if you are saving
        the job to a file, you should save it after this call to `submit`.

        Args:
            job (CCRJob): An object describing a CCR job to submit.

            wait (bool): A flag to indicate whether to block until all jobs
                are done, or to return immediately.
        """

        # submit with sbatch
        res = self.sh.sbatch(job.path / job.submit_file_name)

        # get job id
        idx = res.stdout.rfind(' ')
        job.job_id = res.stdout[idx+1:].strip()

        if(self.verbose):
            print("Job submitted:\njob id = " + str(job.job_id) + "\n")

        if(wait):
            self.block_until_complete(job)


    def retrieve_results(self, job):
        """ 
        Gets simulations results from server.
        
        Gathers simulation results from remote file system, and returns the
        array of these simulation results. Does not check whether this job is
        complete. Thus, this can retrieve partial results

        Args:
            job (CCRJob): The job that was submitted

        Returns:
            A list of objects returned by the user-submitted callable

        """

        RES_TGZ_FILE = "res.tar.gz"

        # Prepare job results on server
        remote_tgz_path = job.path / RES_TGZ_FILE
        self.sh.tgz(remote_tgz_path, job.res_path)

        # download them onto local computer
        dl_path = self.temp_dir / str(uuid.uuid1()))
        self.sh.mkdir(dl_path, local = True)
        local_tgz_path = dl_path / RES_TGZ_FILE
        self.sh.get(remote_tgz_path, local_tgz_path)

        # extract zip file
        with tarfile.open(local_tgz_path, "r:gz") as tar_fp:
            tar_fp.extract(RES_TGZ_FILE, path = dl_path)

        # load results into memory
        res_dir = job.res_path.parts[-1]
        for f in (dl_path / res_dir).iterdir():
            try:
                res = pickle.load(f)
                results.append(res)
            except EOFError as e:
                pass

        return results


    def is_complete(self, job):
        """ 
        Checks whether a job has completed on the server.

        Queries the login server about job status. Returns `True` if job was
        completed, `False` otherwise.

        Args:
            job (CCRJob): The submitted job

        Returns:
            `bool`: `True` if job is complete, `False` otherwise
        """

        res = self.sh.squeue(job)
        return res.stdout.count('\n') == 1


    def num_jobs_user(self, user):
        res = self.sh.squeue_user(user)
        return res.stdout.count('\n') - 1


    def print_status(self, job):
        """ 
        Prints out the status of a job.

        This is essentially a call to `squeue` on ther remote login server.

        Args:
            job (CCRJob): An object describing a CCR job to submit.
        """

        res = self.sh.squeue(job)
        print(res.stdout)


    def block_until_complete(self, job):
        # block until jobs are done
        while(True):
            time.sleep(CCRSubmit._POLLING_FREQ)
            if(self.is_complete(job)):
                return

    def _setup_working_dir(self, job):
        # creates directories and files on remote login node

        # create a new directory for this job submission
        self.sh.mkdir(job.path)
        if(self.verbose):
            print("CCR working directory:\n" + str(job.path))

        # create output directory to hold job results
        self.sh.mkdir(job.path / job.res_dir_name)

        #create src dir to hold app code 
        self.sh.mkdir(job.path / job.src_dir_name)
        self.sh.git_clone(job.branch, job.git_url, job.path / job_src_dir_name)


    def _write_run_script(self, job):
        # Builds the python script that will be called on CCR compute nodes
        # from the template in :class:`CCR`.`_RUN_CODE_PY`

        # apply template
        code = CCRSubmit._RUN_CODE_PY\
                .replace("{NUM_SIMS}", str(job.simulator.num_sims)) \
                .replace("{SRC_PATH}", job.path / job.src_dir_name) \ 
                .replace("{RES_PATH}", job.path / job.res_dir_name)

        # write file to server
        self.sh.cat(code, job.path / job.run_file_name)


    def _write_submit_script(self, job):
        # Builds the shell script that will be submitted to the batch scheduler

        # apply template
        code = CCRSubmit._SUBMIT_CODE_SH\
            .replace("{ENVIRONMENT}", job.environment)\
            .replace("{PY_MODULE}", job.py_module)\
            .replace("{JOB_NAME}", job.job_name)\
            .replace("{JOB_TIME}", job.job_time)\
            .replace("{JOB_DIR}",  str(job.path))\
            .replace("{RUN_FILE_NAME}", job.run_file_name)\
            .replace("{NUM_JOBS}", str(job.num_jobs-1))\
            .replace("{NODES}", str(job.num_nodes))\
            .replace("{CORES}", str(job.num_cores_per_node))\
            .replace("{MEM}", str(job.mem))\
            .replace("{EMAIL}", self.username + "@buffalo.edu")\
            .replace("{MAIL_TYPE}", job.mail_type)\
            .replace("{PARTITION}", job.partition)\
            .replace("{QOS}", job.qos)\

        self.sh.cat(code, job.path / job.submit_file_name)

    # How often to poll server for certain blocking operations
    _POLLING_FREQ = 10


class CCRJob:
    """ 
    Represents a job submission to the SLURM scheduler on CCR.

    Args:
        git_url (`str`): The url to the app-code git repository
        
       
        base_dir (`str`): The absolute path on remote CCR server that will hold
            the working directory for this simulation
        
        environment (`str`): The absolute path on the remote CCR to the python
            virtual enviornment that should be used to run jobs.

        branch (`str`): The name of the branch of the app code. Optional,
            default = "master"
        
        job_name (`str`): A short description (no spaces, less than 100
            characters if possible) of the jobs being run. Optional, default
            = "ccr_job"
        
        num_jobs (`int`): The number of submitted jobs to distribute the
            tasks over. If you are doing :math:`N` tasks and set ``num_jobs``
            to :math:`n`, then each submitted slurm job will perform
            :math:`N/n` tasks. `num_jobs` must be less than or equal to the
            total number of tasks you wish to do.  Because of the overhead in
            finding compute nodes for each job, in general, you should pick
            `num_jobs` so that each job takes at least a few minutes to run.
            *Optional*, default = ``10``.
        
        num_nodes (`int`): The number of nodes for each submitted job.
            *Optional*, default = ``1``.
        
        num_cores_per_node (`int`): The number of cores needed for each job.
            *Optional*, default = ``1``.
        
        mem (`int`): The requested memory for each job, in MB. *Optional*,
            default = ``3000``.
        
        job_time (`str`): The time limit for the job, expressed as a time
            string, HH:mm:ss. *Optional*, default = ``00:15:00``, corresponding
            to 15 minutes.

        partition (`str`): Specifies which queue to submit this job to.
            *Optional*, default = ``"general-compute"``.

        qos (`str`): Specifies quality-of-service provider for this job.
            *Optional*, default = ``"general-compute"``.
    """


    def __init__(self, git_url, base_dir, environment, 
            branch = "master",
            job_name = "ccr_job",
            num_jobs = 10,
            num_nodes = 1,
            num_cores_per_node = 1,
            mem = 3000,
            job_time = "00:15:00",
            partition = "general-compute",
            qos = "general-compute"): 

        self.git_url = git_url
        self.base_dir = base_dir
        self.environment = environment

        self.branch = branch
        self.job_name = job_name
        self.num_jobs = num_jobs
        self.num_nodes = num_nodes
        self.num_cores_per_node = num_cores_per_node
        self.mem = mem
        self.job_time = job_time
        self.mail_type = mail_type
        self.partition = partition
        self.qos = qos 

        self.job_id = None

        # automatically name job dir
        self.prefix = datetime.datetime.now().strftime("%Y.%m.%d.%H.%M.%S")
        self.path = Path(self.base_dir + "/" + 
                         self.prefix + "__" + self.job_name)

        # Aux files and dir, relative to path
        self.res_dir_name = "res"
        self.src_dir_name = "src"
        self.run_file_name = "run.py"
        self.submit_file_name = "submit.sh"


class ParallelCallable():
    def __init__(self, f, kwargs):
        self.f = f
        self.kwargs = kwargs
        
    def __call__(self, start, end):
        return self.f(**self.kwargs, start = start, end = end)


# The python executable run on each compute node
_RUN_CODE_PY = """
import sys
sys.path.insert(0, "{SRC_PATH}")
from ccr_utils.submit import CCRJob, ParallelCallable
from sys import argv
import bz2
import pickle

task_id = int(sys.argv[1])
task_count = int(sys.argv[2])

num_jobs_per_task = int({NUM_JOBS}/task_count)
start = task_id * num_sims_per_task
end = (task_id + 1)*num_sims_per_task

res = par_f(start, end)

save_file_name = "{RES_PATH}/s%04d_e%04d.res" % (start, end)
with bz2.BZ2File(save_file_name, 'wb') as f:
    pickle.dump(res, f)
"""

# The batch submission script
_SUBMIT_CODE_SH = """
#!/bin/sh
#SBATCH --partition={PARTITION}
#SBATCH --qos={QOS}
#SBATCH --time={JOB_TIME}
#SBATCH --nodes={NODES}
#SBATCH --ntasks-per-node={CORES}
#SBATCH --mem={MEM}
#SBATCH --job-name="{JOB_NAME}"
#SBATCH --array=0-{NUM_JOBS}
#SBATCH --output="{JOB_DIR}/%A_%03a.out"
#SBATCH --mail-user={EMAIL}
#SBATCH --mail-type={MAIL_TYPE}
#SBATCH --requeue

#module load {PY_MODULE} > /dev/null 2>&1
source {ENVIRONMENT}/bin/activate
cd {JOB_DIR}
python {RUN_FILE_NAME} $SLURM_ARRAY_TASK_ID $SLURM_ARRAY_TASK_COUNT
"""
