#!/usr/bin/env python

from distutils.core import setup

setup(name='ccr_utils',
      version = '0.1',
      description = """
      Utilities for working with and interfacing with UB's CCR compute
      servers. """,
      author='CSMS Lab',
      author_email='kris@csms.io',
      url='https://bitbucket.org/csmslab/ccr-utils/',
      packages=['ccr_utils'],
      install_requires=[])
